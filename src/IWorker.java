package threesolid;

import java.awt.*;
import java.awt.event.*;  
import java.io.*;

/*interface IWorker {
	public void work();
	public void eat();
}*/

interface IWorker extends Feedable, Workable{
}

interface IWorkable{
	public void work();
}

interface IFeedable{
	public void eat();
}


/*Interface Segregation: Like the webpage from the assignment suggests, splitting the IWorker interface into two seperate interfaces
allows us to more closely adhere to the interface segregation principle. The interface for IWorkable/IFeedable should be seperate,
in case we have a class that doesn't need to implement both, like the robot class for example. This allows us to have a more flexible design.
*/

/*Single Responsibility: While this applies more to classes that have several methods, we SORT of used this when
splitting up the interfaces. Now each interface (IWorkable, IFeedable) are only responsible for one method. This obviosuly applies more
to interface seg but there is still the consideration of single responsibility. */

/*Open-Close: I was unable to see how this principle applied to IWorker at first. I suppose you could say we utilized this
because IWorkable and IFeedable COULD add methods if we wanted to, extending the IWorker interface. IFeedable and IWorker also override the 
work() and eat() functions, allowing for the possiblity of extending the functionality.*/