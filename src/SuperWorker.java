//Israel Carcamo
//Assignment 9
package threesolid;

import java.awt.*;        // Using AWT container and component classes
import java.awt.event.*;  // Using AWT event classes and listener interfaces
import java.io.*;



/*
 The changes I made here is that I seperated the SuperWorker into two other classes that onlcy focuses on one method,
 that way it is following the single responsibility rule. The interface rule is not used in this one because this is
 a class. The open to extension, closed to modification is being used on here when the SuperWorkerWork/Eats is
 implementing the Iworker interface. Israel C.
 */

class SuperWorkerWork implements IWorker {
    public void work() {
        //.... working much more
    }

}

class SuperWorkerEats implements IWorker {
    public void eat() {
        //.... eating in launch break
    }
}