package threesolid;
/*
    class Robot follows the Single responsibility principle
    because it only contains one part of the programs
    functionality.

    Robot is not an interface, but implements IWorkable
    which follows the Interface Segregation Principle.

    Robot class is open for extension of its functionality
    without changes to the existing code. Extending Robot
    further may change whether it follows the Single
    responsibility principle.
*/
class Robot implements IWorkable {
    public void work(){
        // work
    }

}