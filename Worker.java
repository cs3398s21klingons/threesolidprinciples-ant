package threesolid;

import java.awt.*;
import java.awt.event.*; 
import java.io.*;


/*class Worker implements IWorker{
	public void work() {
		// ....working
	}

	public void eat() {
		//.... eating in launch break
	}
}*/


class Worker implements IWorkable, IFeedable{
	public void work() {
		// ....working
	}

	public void eat() {
		//.... eating in launch break
	}
}



/*Single Responsibility: This principle didn't apply here, at least not in the way I modified it. I briefly considered
Splitting the worker class into two seperate one EatingWorker that would handle eat() and WorkingWorker which would handle work(),
but I don't think that really makes sense, it seems silly to split it up that way when a single worker might both eat and work.*/

/*Open Close Principle: This principle applies. Worker includes the functions from IWorkable and IFeedable but adds the implementation
of the method. The interface isnt changed, but implementation is added. */

/*Interface segregation: The principle applies here in that we use two seperate client-specific interfaces instead of a general-purpose
one. It applies more to the Iworker file but since this class implements it then it applies here as well.*/